$(function(){

	$('.reviews-slider').slick({  
		infiniti: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		arrows: false,
		dots:true,
		responsive: [
			{
				breakpoint: 1160,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
		]
	})
		$('.btn__menu').on('click', function(){
			$('.menu__list').slideToggle();
		})
});